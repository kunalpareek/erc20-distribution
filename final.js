var sleep = require('sleep');
var ethers = require('ethers');

var privateKey="0xd3844ee975f6fbb2a172b428cab4198b130198645ba4e3bf1b21bc5bb80fdced";
var contractAddress = "0x12f7B7149B82dEF93936173f4B6Fc2A9355F0DC3";
var address = "0x4EBdbdDFfF6720eBa9E1626cc0644638093F442d"//wallet address

var wallet = new ethers.Wallet(privateKey);
var providers = ethers.providers;
var utils = ethers.utils;
var network = providers.networks.ropsten;
const gas_price_gwei=500;


/*
// for ropsten test net
provider = providers.getDefaultProvider(network);
wallet.provider = providers.getDefaultProvider(network);
*/

provider = providers.getDefaultProvider();
wallet.provider = providers.getDefaultProvider();

const csv=require('csvtojson');
const csvFilePath="1.csv";
var data = [];
csv()
.fromFile(csvFilePath)
.on('json',(jsonObj)=>{
    // combine csv header row and csv line to a json object
    // jsonObj.a ==> 1 or 4
    data.push(jsonObj)
})
.on('done',(error)=>{
    // console.log(data)

    var ul = data.length;
    // console.log(ul);
    var tempCount = 0;


    (function inside () {

        var txdata = "0xa9059cbb"+"000000000000000000000000" + data[tempCount]["address"].slice(2) + create64BitToken(utils.hexlify(parseInt(data[tempCount]["token"])));
        var transaction = {
            gasLimit: 51000 ,
            to: contractAddress,//contract address
            data: String(txdata),
        };
        console.log(transaction);

        wallet.sendTransaction(transaction)
        .then(function(transaction) {
            console.log('Transaction created:'+transaction.hash);

           return provider.waitForTransaction(transaction.hash);
        })
        .then(function(transaction) {
            console.log('Transaction Mined: ' + transaction.hash);
            console.log(transaction.creates);
            tempCount++;
            if(tempCount >= ul)
                return null;
            inside();

            return transaction.creates;
        })
        .catch(function(err){
                console.log(err);
       });
    })();
})

function create64BitToken(token) {
    var len = token.slice(2).length;
    var pre = "";
    var ul = 64 - len + 2
    while (len < ul){
      pre += "0";
      len++;
    }
    return pre + token.slice(2)
}
