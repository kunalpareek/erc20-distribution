var ethers = require('ethers');
var Contract = ethers.Contract;
var privateKey="0xd3844ee975f6fbb2a172b428cab4198b130198645ba4e3bf1b21bc5bb80fdced";
var wallet = new ethers.Wallet(privateKey);
var providers = ethers.providers;
var utils = ethers.utils;
const max_count=50;
var network = providers.networks.ropsten;
const gas_price_gwei=500;
address = "0x4EBdbdDFfF6720eBa9E1626cc0644638093F442d"
wallet.provider = providers.getDefaultProvider(network);

// provider.getBalance(address).then(function(balance) {
//
//     // balance is a BigNumber (in wei); format is as a sting (in ether)
//     var etherString = ethers.utils.formatEther(balance);
//
//     console.log("Balance: " + etherString);
// });


var transaction = {
    // Recommendation: omit nonce; the provider will query the network
    nonce: 10,

    // Gas Limit; 21000 will send ether to another use, but to execute contracts
    // larger limits are required. The provider.estimateGas can be used for this.
    gasLimit: 51000,

    // Recommendations: omit gasPrice; the provider will query the network
    //gasPrice: utils.bigNumberify("20000000000"),

    // Required; unless deploying a contract (in which case omit)
    to: "0x12f7B7149B82dEF93936173f4B6Fc2A9355F0DC3",

    // Optional
    data: "0xa9059cbb000000000000000000000000b69f4d5a4ff1ccb11a7ddb6a91de388820348858000000000000000000000000000000000000000000000000000000000000000a",

    // Optional
    // value: ethers.utils.parseEther("1.0"),

    // Recommendation: omit chainId; the provider will populate this
    // chaindId: providers.Provider.chainId.homestead
};

// Estimate the gas cost for the transaction
//var estimateGasPromise = wallet.estimateGas(transaction);

//estimateGasPromise.then(function(gasEstimate) {
//    console.log(gasEstimate);
//});

// Send the transaction
// var signedTransaction = wallet.sign(transaction);
var sendTransactionPromise = wallet.sendTransaction(transaction);

sendTransactionPromise.then(function(transactionHash) {
    console.log(transactionHash);
});
